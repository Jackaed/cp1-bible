\documentclass{article}
\usepackage[parfill]{parskip}
\usepackage{amssymb}
\usepackage{mathtools}
\author{Ed Jackson}
\title{Edexcel Further Maths Core Pure 1 Bible}

\begin{document}

\maketitle
\tableofcontents
\pagebreak
\section{Complex Numbers}
\subsection{Imaginary Numbers}
An imaginary number is a number which is a multiple of \(i\) where \(i \equiv \sqrt{-1}\). Or, in
mathematical terms \(z = b \sqrt{-1}\ = bi \) where \(b \in \mathbb{R}\).
\subsection{Complex Numbers}
A complex number is a number containing both real and imaginary parts. It is written as: \(z = a +
bi\), where \(a, b \in \mathbb{R}\).

\subsection{Addition and subtraction}
To add or subtract complex numbers, simply add or subtract the complex and real sections of the
complex number.

\subsection{Complex Conjugates}
For a given complex number \(z = a + bi\), z's complex conjugate (referred to as z*) is defined as
\(z^* = a - bi\).

The multiple of a complex number \(z = a + bi\) with it's complex conjugate \(z^*\) is equal to
\(a^2 + b^2\). Note that this is real, not complex.

\subsubsection{Complex Conjugates in Polynomials}
For a quadratic with real coefficients and two distinct complex roots (any complex number with
discriminant less than 0), the roots of form a complex conjugate pair.

If the roots of a quadratic equation are \(\alpha, \beta\), then the quadratic can be written as
\(x^2 - (\alpha + \beta)x + \alpha\beta = 0\).

For any polynomial \(f(x)\) with real coefficients, all complex roots occur in complex conjugate
pairs.

\section{Argand Diagrams}

\subsection{Complex Representation on the Argand Diagram}
A complex number can be represented as a point on an Argand diagram. An Argand Diagram's x axis is
the real component of the complex number, and its y axis is the imaginary component.

\subsection{Modulus-Argument Form of a Complex Number}

The modulus of a complex number, denoted as \(|z|\), is defined as the distance from the origin to
that number's location on an argand diagram. Mathematically: \[z = a + bi \implies |z| = \sqrt{a^2 +
		b^2 }\]

The argument of a complex number is the angle between the real axis and the line joining the complex
number to the origin. Mathematically: \[z = a + bi \implies \tan(\theta) = \frac{a}{b}\] where
\(\theta\)
is the argument of the complex number.

To calculate theta, you must first see which quadrant your complex number is in. For \(z = a + bi\):

If z lies in Q1: \(\arg(z) = \arctan(|\frac{a}{b}|)\)

If z lies in Q2: \(\arg(z) = \pi - \arctan(|\frac{a}{b}|)\)

If z lies in Q3: \(\arg(z) = \arctan(|\frac{a}{b}|) - \pi \)

If z lies in Q4: \(\arg(z) = -\arctan(|\frac{a}{b}|)\)

\subsubsection{Rules of Modulus-Argument form}
For any two complex numbers \(z_1, z_2\):
\begin{itemize}
	\item \(|z_1 z_2| = |z_1||z_2|\)
	\item \(\arg(z_1 z_2) = \arg(z_1) + \arg(z_2)\)
	\item \(|\frac{z_1}{z_2}| = \frac{|z_1|}{|z_2|}\)
	\item \(\arg(\frac{z_1}{z_2}) = \arg(z_1) - \arg(z_2)\)
\end{itemize}

Or, in other words, modulus is commutative, and argument follows log rules for addition and
subtraction.

A complex number can be written in modulus argument form like so: \[z = r(\cos(\theta) +
	i\sin(\theta))\] where \(r = |z|, \theta = \arg(z)\)

It can then be shown that for two complex numbers \(z_1, z_2\): \[z_1 z_2 = r_1 r_2(\cos(\theta_1 +
	\theta_2) + i\sin(\theta_1 + \theta_2))\] where \(r_1 = |z_1|, r_2 = |z_2|, \theta_1 = \arg(z_1),
\theta_2 = \arg(z_2) \)

\subsection{Complex Loci on the Argand Diagram}

The locus of points on an Argand diagram is a sketch of the set of points which meet a particular
condition.

Given \(z_1 = x_1 + iy_1\), the locus of points \(z\) on an argand diagram such that \(|z-z_1| = r\)
forms a circle with centre \((x_1, y_1)\) and radius r.

Given two points, \(z_1 = x_1 + iy_1\) and \(z_2 = x_2 + iy_2\), the locus of points such that
\(|z-z_1| = |z - z_2|\) is the perpendicular bisector of the line segment joining \(z_1\) and
\(z_2\). (This can be thought about as the set of points which are at equal distances to \(z_1\) and
\(z_2\)).

Given \(z_1 = x_1 + iy_1\), the locus of points z such that \(arg(z - z_1) = \theta\) forms a half
line from, but not including the point \(z_1\), making an angle \(\theta\) with the horizontal.

\section{Series}
For sequences and series, there are a number of critical formulae you must learn:
\[\sum_{r=1}^{n}1 \equiv n\]
For n natural numbers:
\[\sum_{r=1}^{n}r \equiv \frac{1}{2}n(n+1)\]
For the sum of a series starting with \(r=k\):
\[\sum_{r=k}^{n}f(r) \equiv \sum_{r=1}^{n}f(r) - \sum_{r=1}^{k-1}f(r)\]
For the sum of the squares of the first n natural numbers:
\[\sum_{r=1}^{n}r^2 \equiv \frac{1}{6}n(n+1)(2n+1)\]
For the sum of the cubes of the first n natural numbers:
\[\sum_{r=1}^{n}r^3 \equiv \frac{1}{4}n^2(n+1)^2\]
Which is coincidentally equal to the sum of the natural numbers squared.

\subsection{Rearranging Series}
You can take constant terms outside of the summation:
\[\sum k f(r) \equiv k \sum f(r)\]

And you can split additions into multiple summations:

\[\sum f(r) + g(r) \equiv \sum f(r) + \sum g(r)\]

However, you cannot split multiplications in the same way:

\[\sum f(r)g(r) \not\equiv \sum f(r) \sum g(r) \]

\section{Roots of Polynomials}

If a polynomial has roots \(\alpha, \beta, \gamma\)\ldots and coefficients \(a, b, c\)\ldots:
\[\alpha + \beta + \gamma + \ldots = - \frac{b}{a}\]
\[\alpha\beta + \beta\gamma + \alpha\gamma +\ldots = \frac{c}{a}\]
\[\alpha\beta\gamma + \ldots = -\frac{d}{a}\]

More generally, this pattern repeats for any polynomial. You can see that as you move down the
pattern, the multiplied expressions increase in the number of multiplied items. On the right hand
side, the sign alternates, and the denominator moves down the coefficient order.

These rules tend to be written as:
\[\Sigma \alpha = - \frac {b}{a}\]
\[\Sigma \alpha\beta = \frac{c}{a}\]
\[\Sigma \alpha\beta\gamma = -\frac{d}{a}\ldots\]

\subsection{Sum of the Squares}
The following rule applies:
\begin{itemize}
	\item Quadratic: \(\alpha ^ 2 + \beta ^ 2 = (\alpha + \beta)^2 - 2\alpha\beta\)
	\item Cubic: \(\alpha ^ 2 + \beta ^ 2 + \gamma ^ 2 = (\alpha + \beta + \gamma)^2 - 2(\alpha\beta\ +
	      \beta \gamma + \gamma \alpha)\)
\end{itemize}

This can be written generally for any polynomial:
\[\alpha ^ 2 + \beta ^ 2 + \gamma ^ 2 + \ldots = (\Sigma \alpha)^2 - 2 (\Sigma \alpha \beta)\]

\subsection{Sum of the Cubes}
Unfortunately, this does not follow a pattern, and must be learned:
\begin{itemize}
	\item Quadratic: \(\alpha^3 + \beta^3 = (\alpha + \beta)^3 -3\alpha\beta(\alpha + \beta)\)
	\item Cubic: \(\alpha^3 + \beta^3 + \gamma^3 = (\alpha + \beta + \gamma)^3 -3(\alpha + \beta +
	      \gamma)(\alpha\beta + \beta\gamma + \gamma\alpha) + 3\alpha\beta\gamma\)
\end{itemize}
Note that the cubic follows the pattern of the quadratic, but has a final added correction term.

\section{Volumes of Revolution}
The total volume produced by rotating a positive curve around the x axis by \(2\pi\) radians between two x limits can be
calculated using:
\[V = \pi \int_{a}^{b}y^2 dx\]

Similarly, when rotating around the y axis:

\[V = \pi \int_{a}^{b}x^2 dy\]

Remember that the limits that are applied occur around the axis which you are rotating around

\section{Matrices}

A square matrix is one with an equal number of rows and columns

A zero matrix contains only zeroes, and an Identity matrix contains all zeroes except ones on the
leading diagonal.

To add or subtract matrices, add or subtract the corresponding elements in each matrix.

To multiply a matrix by a scalar, multiply each element in the matrix by that scalar.

To multiply two matrices, multiply th elements in each row in the left hand matrix by the
corresponding elements in each column in the right hand matrix, then add the results together. For
example:

\[ \left( \begin{array}{cc}
			a & b \\ c & d
		\end{array} \right) \times
	\left( \begin{array} {cc}
			e & f \\ g & h
		\end{array}\right) =
	\left( \begin{array}{cc}
			ae + bg & af + bh \\
			ce + dg & cf + dh
		\end{array} \right) \]

This imposes the restriction that you can only multiply two matrices if the second matrix has as
many rows as the first has columns. Two matrices that can be multiplied means that the first is said
to be multiplicatively conformable with the second.

For a 2 by 2 matrix \(\left( \begin{array}{cc}
		a & b \\
		c & d
	\end{array}\right)\), the determinant of the matrix is \(ad-cb\).

A matrix is ``singular'' if it's determinant is 0, as it does not have an inverse matrix.

The determinant of a 3x3 matrix can be calculated through calculating the determinants of 2x2
matrices on the bottom of the matrix. Specifically:

\[ \left| \begin{array}{ccc}
	a & b & c \\ d & e & f \\ g & h & i
\end{array} \right| = a \left| \begin{array}{cc}
	e & f \\ h & i 
\end{array} \right| - b \left| \begin{array}{cc}
	d & f \\ g & i 
\end{array} \right| + c \left| \begin{array}{cc}
	d & e \\ g & h 
\end{array} \right|\]

Each of these sub-matrices is referred to as a ``minor'' of the element which it is multiplied by.
More generally, a minor of an element is calculated by crossing out the row and column that an
element is on.

The inverse of a matrix \(M\) is such that \(MM^{-1} = I\)

For a 2x2 matrix, if \[M = \left( \begin{array}{cc}
	a & b \\ c & d
\end{array} \right)\]

then \[M^{-1} = \frac{1}{det(M)} \left( \begin{array}{cc}
	d & -b \\ -c & a
\end{array} \right)\]

This can be remembered by flipping the elements along the leading diagonal and changing the sign on
the off diagonal.

If A and B are non singular: \((AB)^{-1} = A^{-1} B^{-1}\)

To calculate the inverse of a 3x3 matrix, calculate it's matrix of minors (replace each element with
it's minor), then reflect the matrix along the leading diagonal, and apply the rule of alternating
signs (for every odd-numbered element, flip the sign).

You can use matrices to solve simultaneous equations, since:

\[A \left( \begin{array}{c}
	x \\ y \\ z
\end{array}\right) = M \implies \left( \begin{array}{c}
	x \\ y \\ z
\end{array}\right) = A^{-1}M\]

A set of simultaneous equations is consistent if it has at least one set of values that satisfy all
equations. Otherwise, it is inconsistent.


\end{document}